export default {
  night: 'Nuit',
  nights: 'Nuits',
  week: 'Semaine',
  weeks: 'Semaines',
  'check-in': 'Départ',
  'check-out': 'Arrivée',
  'day-names': ['Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa', 'Di'],
  'month-names': [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
  ],
  tooltip: {
    halfDayCheckIn: 'Réservation possible',
    halfDayCheckOut: 'Réservation possible',
    saturdayToSaturday: 'Du samedi au samedi uniquement',
    sundayToSunday: 'Du dimanche au dimanche uniquement',
    fridayToFriday: 'Du vendredi au vendredi uniquement',
    fridayToMonday: 'Only Friday to Monday',
    monToFridayOnly: 'Only Monday to Friday',
    minimumRequiredPeriod: '%{minNightInPeriod} %{night} minimum',
    sevenOrMore: 'Check in only on Friday for seven or ten days',
  },
}
